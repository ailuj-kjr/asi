let cardList = [];

function getCards() {
    const xhr = new XMLHttpRequest();
	let url = "http://localhost:8080/api/cards?uid="+userID
    xhr.open("GET", url, true);
    // When response is ready
    xhr.onload = function() {
        if (this.status === 200) {
            // Changing string data into JSON Object
            obj = JSON.parse(this.responseText);
            cardList = obj;
            displayCards();
        } else {
            console.log("File not found");
        }
    }

    // At last send the request
    xhr.send();

}

//TODO
function sellCard(cardID){
 	const xhr = new XMLHttpRequest();
    xhr.open("DELETE", "http://localhost:8080/api/cards/"+cardID+"?uid="+userID, true);
    xhr.onload = function() {
        if (this.status === 200) {
			console.log(this.responseText)
			window.location.reload();
        } else {
            console.log("File not found");
        }
    }
    xhr.send();


}


function displayCards() {
    let template = document.querySelector("#row");
    
    if(!cardList){
    	document.getElementById("error_card").hidden = false;
    }else{
    
    	if(cardList.length == 0){
    	 document.getElementById("error_card").hidden = false;
    	 return;
    	 }
    
     for (const card of cardList) {
        let clone = document.importNode(template.content, true);

        newContent = clone.firstElementChild.innerHTML
            .replace(/{{family}}/g, card.family)
            .replace(/{{img_src}}/g, card.img_src)
            .replace(/{{name}}/g, card.name)
            .replace(/{{description}}/g, card.description)
            .replace(/{{hp}}/g, card.hp)
            .replace(/{{energy}}/g, card.energy)
            .replace(/{{attack}}/g, card.attack)
            .replace(/{{defense}}/g, card.defense)
            .replace(/{{price}}/g, card.price);
            
        clone.firstElementChild.innerHTML = newContent;
        
        clone.firstElementChild.lastElementChild.onclick = function() {sellCard(card.id)};
        let cardContainer = document.querySelector("#tableContent");
        cardContainer.appendChild(clone);
    	}
    }   
}

getCards();