// wait for doc to load
$(document).ready(function() {
    //get the form
    var form = $('#register-form');
    //get the username input
    var username = $('#username');
    //get the password input
    var password = $('#password');
    // get the confirm password input
    var confirmPassword = $('#confirm-password');
    // get the email input
    var email = $('#email');
    //get the error message
    var error = $('#error');

    form.submit(function(event) {
        //prevent default php processing
        event.preventDefault();
        //clear the error message
        error.html('');
        //check if username is empty
        if (username.val() == '') {
            error.html('<div class="alert alert-danger">Please enter your username</div>');
        }
        //check if password is empty
        else if (password.val() == '') {
            error.html('<div class="alert alert-danger">Please enter your password</div>');
        }
        //check if confirm password is empty
        else if (confirmPassword.val() == '') {
            error.html('<div class="alert alert-danger">Please confirm your password</div>');
        }
        //check if email is empty
        else if (email.val() == '') {
            error.html('<div class="alert alert-danger">Please enter your email</div>');
        }
        //check if password and confirm password are not the same
        else if (password.val() != confirmPassword.val()) {
            error.html('<div class="alert alert-danger">Passwords do not match</div>');
        }
        //if username and password are not empty
        else {
            //send data to server using ajax
            $.ajax({
                type: 'POST',
                url: 'api/users',
                dataType: 'json',
                contentType: 'application/json',
                encode: true,
                data: JSON.stringify({
                    username: username.val(),
                    email: email.val(),
                    password: password.val()
                })
            })
            //if response is true
            .done(function(data) {
                //if response is true
                if (data) {
                    //redirect to home page
                    window.location.href = '/';
                }
                //if response is false
                else {
                    //display error message
                    error.html('<div class="alert alert-danger">' + data.responseText + '</div>');
                }
            })
            //if error
            .fail(function(data) {
                //display error message
                error.html('<div class="alert alert-danger">' + data.responseText + '</div>');
            });
        }
    });
});

