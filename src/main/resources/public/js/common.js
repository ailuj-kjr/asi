let user = undefined; 
let userID = 1;


 $(function(){
 	$("#userdiv").load("./part/user.html");
 	getUser();
 });

 
function getUser() {
    const xhr = new XMLHttpRequest();

    xhr.open("GET", "http://localhost:8080/api/users/"+userID, true);
    // When response is ready
    xhr.onload = function() {
        if (this.status === 200) {
        
            let obj = JSON.parse(this.responseText);
            user = obj;
            
            displayUser();
            
        } else {
            console.log("File not found");
        }
    }

    // At last send the request
    xhr.send();
}

function displayUser() {
	document.getElementById("userNameId").innerText = user.username;
	document.getElementById("userMoney").innerText = user.money;
    document.getElementById("user_profile").hidden = false;
}
