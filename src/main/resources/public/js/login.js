// wait for doc to load
$(document).ready(function() {
    // get the form
    var form = $('#login-form');
    // get the username input
    var username = $('#username');
    // get the password input
    var password = $('#password');
    // get the error message
    var error = $('#error');

    form.submit(function(event) {
        // prevent default php processing
        event.preventDefault();
        // clear the error message
        error.html('');
        // check if username is empty
        if (username.val() == '') {
            error.html('<div class="alert alert-danger">Please enter your username</div>');
        }
        // check if password is empty
        else if (password.val() == '') {
            error.html('<div class="alert alert-danger">Please enter your password</div>');
        }
        // if username and password are not empty
        else {
            // send data to server using ajax
            $.ajax({
                type: 'GET',
                url: `api/users/login?username=${username.val()}&password=${password.val()}`,
                dataType: 'json',
                encode: true
            })
            // if response is true
            .done(function(data) {
                // if response is true
                if (data) {
                    // redirect to home page
                    window.location.href = '/';
                }
                // if response is false
                else {
                    // display error message
                    error.html('<div class="alert alert-danger">Invalid username or password</div>');
                }
            })
            // if error
            .fail(function(data) {
                // display error message
                error.html('<div class="alert alert-danger">' + data.responseText + '</div>');
            });
        }
    });
});

