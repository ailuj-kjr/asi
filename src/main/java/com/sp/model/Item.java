package com.sp.model;

public class Item {
    private Card card;

    public Item() {
    }

    public Item(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return this.card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @Override
    public String toString() {
        return "{" +
            " card='" + getCard() + "'" +
            "}";
    }

}
