package com.sp.model;

import javax.persistence.*;

@Entity
public class Stock {

    @Id
    private long cardId;
    
    public Stock(Card card, Integer quantity, Float price) {
        this.card = card;
        this.quantity = quantity;
        this.price = price;
    }

    public Stock() {
    }

    @OneToOne
    @JoinColumn(name = "card_id", nullable = false)
    private Card card;

    private Integer quantity;
    private Float price;

    public Card getCard() {
        return this.card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Float getPrice() {
        return this.price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
