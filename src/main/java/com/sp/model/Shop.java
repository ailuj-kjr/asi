package com.sp.model;

import java.util.List;

public class Shop implements Trade{
    private List<Item> items;

    public Shop(List<Item> items) {
        this.items = items;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public boolean buy(Card card) {
        boolean ret = false;
        for (Item item : items) {
            if (item.getCard().equals(card)) {
                ret = true;
                items.remove(item);
                break;
            }
        }
        return ret;
    }

    @Override
    public boolean sell(Item item) {
        items.add(item);
        return true;
    }
}
