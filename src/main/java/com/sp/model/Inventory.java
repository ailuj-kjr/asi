package com.sp.model;

import javax.persistence.*;

@Entity
public class Inventory {
    @EmbeddedId
    private InventoryId id;

    public Inventory(InventoryId id, long quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    public Inventory(long uid, long cid, long quantity) {
        this.id = new InventoryId(uid, cid);
        this.quantity = quantity;
    }

    public Inventory() {
    }

    private long quantity;

    public InventoryId getId() {
        return this.id;
    }

    public void setId(InventoryId id) {
        this.id = id;
    }

    public long getQuantity() {
        return this.quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }
}
