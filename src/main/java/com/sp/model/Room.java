package com.sp.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;

@Entity
public class Room {
    @Id
    @GeneratedValue
    private Integer id;
    private Integer creatorId;
    private Integer adversaryId;
    private Integer bet;
    private Integer creatirCardId;
    private Integer adversaryCardId;

    public Room() {
    }

    public Room(Integer id, Integer creatorId, Integer adversaryId, Integer bet, Integer creatirCardId,
            Integer adversaryCardId) {
        this.id = id;
        this.creatorId = creatorId;
        this.adversaryId = adversaryId;
        this.bet = bet;
        this.creatirCardId = creatirCardId;
        this.adversaryCardId = adversaryCardId;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCreatorId() {
        return this.creatorId;
    }

    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }

    public Integer getAdversaryId() {
        return this.adversaryId;
    }

    public void setAdversaryId(Integer adversaryId) {
        this.adversaryId = adversaryId;
    }

    public Integer getBet() {
        return this.bet;
    }

    public void setBet(Integer bet) {
        this.bet = bet;
    }

    public Integer getCreatirCardId() {
        return this.creatirCardId;
    }

    public void setCreatirCardId(Integer creatirCardId) {
        this.creatirCardId = creatirCardId;
    }

    public Integer getAdversaryCardId() {
        return this.adversaryCardId;
    }

    public void setAdversaryCardId(Integer adversaryCardId) {
        this.adversaryCardId = adversaryCardId;
    }

    @Override
    public String toString() {
        return "{" +
                " id='" + getId() + "'" +
                ", creatorId='" + getCreatorId() + "'" +
                ", adversaryId='" + getAdversaryId() + "'" +
                ", bet='" + getBet() + "'" +
                ", creatirCardId='" + getCreatirCardId() + "'" +
                ", adversaryCardId='" + getAdversaryCardId() + "'" +
                "}";
    }

}
