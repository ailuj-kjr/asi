package com.sp.model;

public enum Family {
    COMMON,
    SPECIAL,
    RARE,
    EPIC,
    LEGENDARY,
    MYSTICAL
}
