package com.sp.repository;

import com.sp.model.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<Stock, Integer> {

    public Stock findById(int id);

    public Stock save(Stock stock);

    public void delete(Stock stock);
}
