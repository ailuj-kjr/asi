package com.sp.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

import com.sp.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	
	public List<User> findAll();

	public User save(User user);

    public User findById(int id);

	public void deleteById(int id);
    
	public User findByUsername(String username);
}