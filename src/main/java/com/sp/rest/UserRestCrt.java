package com.sp.rest;

import java.util.ArrayList;

import com.sp.model.User;
import com.sp.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class UserRestCrt {
    @Autowired
    UserService userService;

    /*
    * GET /users
    */
    @RequestMapping(method=RequestMethod.GET,value="")
    public ArrayList<User> getUsers() {
        ArrayList<User> users=userService.getUsers();
        return users;
    }

    /*
     * POST /users
     */
    @RequestMapping(method=RequestMethod.POST,value="")
    public boolean addUser(@RequestBody User user) {
        return userService.addUser(user);
    }

    /*
     * GET /users/:uid
     */
    @RequestMapping(method=RequestMethod.GET,value="/{id}")
    public User getUser(@PathVariable String id) {
        User user = userService.getUserById(Integer.valueOf(id));
        return user;
    }

    /*
     * GET /users/login?username=username&password=password
     */
    @RequestMapping(method=RequestMethod.GET,value="/login")
    public User login(@RequestParam String username, @RequestParam String password) {
        User user = userService.login(username, password);
        return user;
    }

    /*
     * DELETE /users/:id
     */
    @RequestMapping(method=RequestMethod.DELETE,value="/{id}")
    public boolean deleteUser(@PathVariable String id) {
        return userService.deleteUser(Integer.valueOf(id));
    }

    /*
     * PUT /users/:id
     */
    @RequestMapping(method=RequestMethod.PUT,value="/{id}")
    public User updateUser(@PathVariable String id, @RequestBody User user) {
        return userService.updateUser(Integer.valueOf(id), user);
    }
}
