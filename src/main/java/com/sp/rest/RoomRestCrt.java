package com.sp.rest;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

  
import com.sp.model.Room;

import com.sp.service.RoomService;

@RestController
@RequestMapping("/api/rooms")
public class RoomRestCrt {
    @Autowired
    RoomService roomService;
    
    /*
    * GET /rooms
    */
    @RequestMapping(method=RequestMethod.GET,value="")
    public ArrayList<Room> getRooms() {
        ArrayList<Room> rooms=roomService.getRooms();
        return rooms;
    }
    
    /*
    * POST /rooms
    */
    @RequestMapping(method=RequestMethod.POST,value="")
    public boolean addRoom(@RequestBody Room room) {
        System.out.println("rooms : " + room);
        return roomService.addRoom(room);
    }
    
    /*
     * GET /rooms/:id
     */
    @RequestMapping(method=RequestMethod.GET,value="/{id}")
    public Room getRoom(@PathVariable String id) {
        Room room = roomService.getRoomById(Integer.valueOf(id));
        return room;
    }
    
    /*
     * DELETE /rooms/:id
     */
    @RequestMapping(method=RequestMethod.DELETE,value="/{id}")
    public boolean deleteRoom(@PathVariable String id) {
        return roomService.deleteRoom(Integer.valueOf(id));
    }

    /*
     * PUT /rooms/:id
     */
    @RequestMapping(method=RequestMethod.PUT,value="/{id}")
    public boolean updateRoom(@PathVariable String id, @RequestBody Room room) {
        return roomService.updateRoom(Integer.valueOf(id), room);
    }
    
}
