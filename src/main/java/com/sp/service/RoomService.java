package com.sp.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.model.Room;
import com.sp.repository.RoomRepository;

@Service
public class RoomService {
	@Autowired
	RoomRepository roomRepository;

	public boolean addRoom(Room room) {
		try {
			roomRepository.save(room);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public ArrayList<Room> getRooms() {
		ArrayList<Room> rooms = new ArrayList<>();
		Iterable<Room> itRoom = roomRepository.findAll();
		for (Room room : itRoom) {
			rooms.add(room);
		}
		return rooms;
	}

	public Room getRoomById(int id) {
		return roomRepository.findById(id);
	}

	public boolean deleteRoom(int id) {
		try {
			roomRepository.delete(getRoomById(id));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean updateRoom(int id, Room room) {
		try {
			room.setId(id);
			roomRepository.save(room);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
