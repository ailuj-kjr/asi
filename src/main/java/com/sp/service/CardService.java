package com.sp.service;

import java.util.ArrayList;
import java.util.List;

import com.sp.model.Family;
import com.sp.model.Inventory;
import com.sp.model.InventoryId;
import com.sp.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.model.Card;
import com.sp.repository.CardRepository;
import com.sp.repository.InventoryRepository;

@Service
public class CardService {
	@Autowired
	CardRepository cardRepository;
	@Autowired
	InventoryRepository inventoryRepository;

	public boolean addCard(Card card) {
		try {
			cardRepository.save(card);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean addCardToUser(long uid, long cid) {
		try {
			Inventory inventory = inventoryRepository.findById(new InventoryId(uid, cid));
			if (inventory == null) {
				inventory = new Inventory(uid, cid, 1);
				System.out.println(inventoryRepository.save(inventory));
			} else {
				inventory.setQuantity(inventory.getQuantity() + 1);
				inventoryRepository.save(inventory);
			}
			return true;
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
	}

	public boolean removeCardToUser(long uid, long cid) {
		try {
			Inventory inventory = inventoryRepository.findById(new InventoryId(uid, cid));
			if (inventory == null)
				return false;
			else {
				inventory.setQuantity(inventory.getQuantity() - 1);
				if (inventory.getQuantity() > 0)
					inventoryRepository.save(inventory);
				else
					inventoryRepository.delete(inventory);
			}
			return true;
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
	}

	public ArrayList<Card> getCards() {
		ArrayList<Card> cards = new ArrayList<>();
		Iterable<Card> itCard = cardRepository.findAll();
		for (Card card : itCard) {
			cards.add(card);
		}
		return cards;
	}

	public ArrayList<Card> getCardsByUser(long uid) {
		ArrayList<Card> cards = new ArrayList<>();
		Iterable<Inventory> itCard = inventoryRepository.findByUser(uid);
		for (Inventory inv : itCard) {
			System.out.println(inv);
			for (int i = 0; i < inv.getQuantity(); i++) {
				cards.add(cardRepository.findById(inv.getId().getCid()));
			}
		}
		return cards;
	}

	public Card getCardById(long id) {
		return cardRepository.findById(id);
	}

	public Card getCardByName(String name) {
		return cardRepository.findByName(name);
	}

	public List<Card> getCardsByFamily(Family family) {
		return cardRepository.findByFamily(family);
	}

	public boolean deleteCard(long id) {
		try {
			cardRepository.delete(getCardById(id));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean updateCard(long id, Card card) {
		try {
			card.setId(id);
			cardRepository.save(card);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}