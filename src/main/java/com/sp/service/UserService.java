package com.sp.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.model.User;
import com.sp.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;



    public User login(String username, String password) {
        User user = userRepository.findByUsername(username);
        if (user != null && user.verifyPassword(password)) {


            return user;
        }
        return null;
    }

    public ArrayList<User> getUsers() {
		ArrayList<User> users = new ArrayList<>();
		Iterable<User> itUser = userRepository.findAll();
		for (User user : itUser) {
			users.add(user);
		}
		return users;
	}

    public boolean addUser(User user) {
        try {
            userRepository.save(user);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public User getUserById(int id) {
        return userRepository.findById(id);
    }

    public boolean deleteUser(int id) {
		try {
			userRepository.delete(getUserById(id));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

    public User updateUser(int id, User user) {
        User oldUser = getUserById(id);
        oldUser.setUsername(user.getUsername());
        oldUser.setMail(user.getMail());
        return userRepository.save(oldUser);
    }
}
