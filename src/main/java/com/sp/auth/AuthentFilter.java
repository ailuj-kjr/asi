package com.sp.auth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.FilterChain;
import java.io.IOException;
import javax.servlet.ServletException;

import org.springframework.web.filter.OncePerRequestFilter;

public class AuthentFilter extends OncePerRequestFilter {
    List<String> whiteList = Arrays.asList("/login", "/register");

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        String uri = request.getRequestURI();
        String tokenJwt = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            tokenJwt = Arrays.stream(cookies)
                        .filter(cookie -> "JWT".equals(cookie.getName()))
                        .findFirst()
                        .map(cookie -> cookie.getValue())
                        .orElse(null);
        }
        if(tokenJwt == null){
            response.sendRedirect("/login");
            //response.setStatus(401);
        }
        filterChain.doFilter(request, response);
    }
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return whiteList.contains(request.getServletPath());
    }
}
