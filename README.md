Routes HTTP : 

- Users : 
    - [x] GET /users (getAll)
    - [x] POST /users (register)
    - User : 
      - [ ] GET /users/login (login)
      - [ ] GET /users/:uid (getInfosById)
      - [ ] DELETE /users/:uid (delete)
      - [ ] PUT /users/:uid (update)
- Cards :
    - [x] GET /cards
    - [x] POST /cards
    - Card :
        - [x] GET /cards/:cid
        - [x] DELETE /cards/:cid 
        - [x] PUT /cards/:cid
- User cards : 
    - [x] GET /cards?uid=uid
    - [x] POST /cards/:cid?uid=uid
    - [x] DELETE /cards/:cid?uid=uid
- Rooms :
    - [ ] GET /rooms
    - [ ] POST /rooms 
    - Room : 
        - [ ] GET /rooms/:rid
        - [ ] DELETE /rooms/:rid
        - [ ] PUT /rooms/:rid -> update du bait, du joueur, ou du gagnant en fin de combat

Améliorations : 

- [ ] Le prix stocké dans le modèle de la carde (cf diagramme de classe )
- [ ] Le prix peut etre multiplié par exemple 1.25*attack ... 
- [ ] BDD : lié la table inventory & stock : IF userid null alors la carte est à vendre


# TODO :

## Controller
- [ ] WebController 
- [ ] CardRestController
- [ ] UserRestController
- [ ] RoomRestController
- [ ] FightRestController

## Service
- [ ] WebService 
- [ ] CardService
- [ ] UserService
- [ ] RoomService
- [ ] FightService

## Repository 
- [ ] WebRepository 
- [ ] CardRepository
- [ ] UserRepository
- [ ] RoomRepository
- [ ] FightRepository

## Front
- [ ] Affichage des cartes
- [ ] Achat d'une carte
- [ ] Vente d'une carte

- [ ] Connection utilisateur
- [ ] Création utilisateur

- [ ] Affichage des rooms


- [ ] Mettre en place la base de données
- [ ] Mettre en place les JWT ( token d'authentification ) 
- [ ] Mise en place des tests de sécurités
- [ ] MAJ des diagrammes 


# Port : 
- 80 : View
- 8080 : User
- 8081 : Carte
- 8082 : Image Service
- 8083 : Rooms
- 8084 : Fight
